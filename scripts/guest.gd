class_name Guest
extends Node3D


var seating: Seating

var arriving: bool = false
var leaving: bool = false
var speed: float
var starting_position: Vector3
var leaving_distance: float = 0
var ordered: bool = false

@onready var game_state: GameState = get_node("/root/GameStateSingleton")

func _physics_process(delta):
    if !game_state.paused:
        if arriving:
            position = position.lerp(Vector3.ZERO, delta * speed)
            if position.length() < 0.2 and !ordered:
                seating.guest_ready_to_order(self)
                ordered = true
            if position.length() < 0.1:
                arriving = false
        if leaving:
            leaving_distance += speed * delta
            position = Vector3.ZERO.lerp(starting_position, leaving_distance)
            if (position - starting_position).length() < 0.5:
                queue_free()


func arrive(parent_seating: Seating, start_speed: float):
    starting_position = position
    seating = parent_seating
    speed = start_speed
    arriving = true


func leave():
    speed *= 0.25
    leaving = true