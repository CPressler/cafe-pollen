class_name GameState
extends Node

var mode: GameManager.GameMode
var paused: bool = false
var money: float = 0
var score: int = 0
var level: int = 0