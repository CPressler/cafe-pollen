class_name Player
extends CharacterBody3D


@export var level_manager: LevelManager
@export var ui_manager: UIManager

# How fast the player moves in meters per second.
@export var speed = 6
@export var entered_keys_to_store = 10
@export var max_inventory_size = 4

@onready var game_state: GameState = get_node("/root/GameStateSingleton")

var last_entered_keys: Array[String] = []
var target_velocity = Vector3.ZERO
var ignored_cooking_keys: Array[String] = ["W", "A", "S", "D"]
var inventory: Array[Recipe] = []

func _ready():
	clear_last_entered_keys()


func _physics_process(_delta):
	if Input.is_action_just_pressed("build"):
		level_manager.toggle_build_mode()
	if Input.is_action_just_pressed("pause"):
		level_manager.toggle_pause()

	if game_state.paused:
		return
		
	var direction = Vector3.ZERO

	if Input.is_action_pressed("move_right"):
		direction.x += 1
	if Input.is_action_pressed("move_left"):
		direction.x -= 1
	if Input.is_action_pressed("move_down"):
		direction.z += 1
	if Input.is_action_pressed("move_up"):
		direction.z -= 1
	if Input.is_action_just_pressed("interact"):
		interact()



	if direction != Vector3.ZERO:
		direction = direction.normalized()
		look_at(position + direction, Vector3.UP)

	# Ground Velocity
	target_velocity.x = direction.x * speed
	target_velocity.z = direction.z * speed

	# Moving the Character
	velocity = target_velocity
	move_and_slide()


func _input(event):
	if event is InputEventKey and event.pressed and !event.echo:
		var pressed_key = OS.get_keycode_string(event.keycode)
		if pressed_key not in ignored_cooking_keys:
			add_entered_key(pressed_key)
			level_manager.player_entered_key(last_entered_keys)


func interact():
	level_manager.player_wants_interaction()


func clear_last_entered_keys():
	last_entered_keys = []
	for i in range(0, entered_keys_to_store):
		last_entered_keys.append("")


func add_entered_key(key):
	last_entered_keys.append(key)
	if last_entered_keys.size() > entered_keys_to_store:
		last_entered_keys.pop_front()


func add_to_inventory(recipe: Recipe):
	if inventory.size() == max_inventory_size:
		return
	inventory.append(recipe)
	ui_manager.add_to_inventory(recipe)


func remove_last_from_inventory():
	remove_from_inventory(inventory.size() - 1)


func remove_from_inventory(index: int):
	if index < 0:
		return

	inventory.pop_at(index)
	ui_manager.remove_from_inventory(index)


func clear_inventory():
	inventory.clear()
	ui_manager.clear_inventory()


func check_if_order_complete(order: Array[Recipe]):
	for needed_recipe in order:
		if !inventory.has(needed_recipe):
			return false
			
	return true


func remove_order(order: Array[Recipe]):
	for recipe in order:
		remove_from_inventory(inventory.find(recipe))
