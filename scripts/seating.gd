class_name Seating
extends Buyable


var possible_pollen: Array[Node3D]
var occupied = false
var ready_to_order = false
var eating = false

var eating_time: float
var eating_since: float

var order: Array[Recipe]

var free_seats: Array[Node3D]
var used_seats: Array[Node3D]

@onready var game_state: GameState = get_node("/root/GameStateSingleton")


func _ready():
	super()
	find_possible_pollen(self)
	for found_possible_pollen in possible_pollen:
		free_seats.append(found_possible_pollen)
	interaction_area.body_entered.connect(body_entered_interaction_area)
	interaction_area.body_exited.connect(body_exited_interaction_area)


func _process(delta):
	if !game_state.paused:
		if eating:
			eating_since += delta
			if eating_since > eating_time:
				clear_guests()


# Workaround for Godot array bug
func find_possible_pollen(node: Node):
	if node.name == "PollenParent":
		possible_pollen.append(node as Node3D)
	elif node.get_child_count() > 0:
		for child in node.get_children():
			find_possible_pollen(child)


func get_possible_guest_count():
	var number_of_free_seats = free_seats.size()

	if number_of_free_seats < 2:
		return number_of_free_seats

	return (randi() % free_seats.size()) + 1


func spawn_guests(guest_scenes: Array[PackedScene], allowed_pollen_deviation: float, pollen_spawn_height: float, min_guest_speed: float, max_guest_speed: float, guest_eating_time: float):
	var guests_to_spawn: int = get_possible_guest_count()
	occupied = true
	eating_time = guest_eating_time

	for i in range(0, guests_to_spawn):
		var pollen_scene = guest_scenes[randi_range(0, guest_scenes.size() - 1)]
		var guest = pollen_scene.instantiate() as Guest
		var seat = free_seats.pop_at(randi() % free_seats.size())
		seat.add_child(guest)
		used_seats.append(seat)

		var x_spawn_offset = randf_range(-allowed_pollen_deviation, allowed_pollen_deviation)
		var z_spawn_offset = randf_range(-allowed_pollen_deviation, allowed_pollen_deviation)
		var pollen_spawn_offset = Vector3(x_spawn_offset, pollen_spawn_height, z_spawn_offset)
		var pollen_speed = randf_range(min_guest_speed, max_guest_speed)

		guest.position = pollen_spawn_offset
		guest.arrive(self, pollen_speed)


func clear_guests():
	for used_seat in used_seats:
		var guest = used_seat.get_child(0)
		guest.leave()
		free_seats.append(used_seat)
	used_seats.clear()
	eating = false
	eating_since = 0
	eating_time = 0
	occupied = false



func guest_ready_to_order(_guest: Guest):
	if !ready_to_order:
		ready_to_order = true
		order = cafe.generate_order()
		cafe.seating_changed_order(self)


func body_entered_interaction_area(_body: Node3D):
	cafe.player_entered_seating(self)


func body_exited_interaction_area(_body: Node3D):
	cafe.player_exited_seating(self)


func order_delivered():
	ready_to_order = false
	order.clear()
	eating = true
