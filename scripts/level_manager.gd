class_name LevelManager
extends Node


enum Activity {
	PAUSE,
	SERVE,
	COOK,
	BUILD,
	GAME_OVER
}

@export var cafe: Cafe
@export var uiManager: UIManager
@export var player: Player
@export var ui_manager: UIManager

@export var start_min_seconds_until_spawn: float = 9.0
@export var min_min_seconds_until_spawn: float = 3.0
@export var seconds_until_spawn_range: float = 3.0

@export var guests_per_stage: int = 3
@export var time_change_per_stage: int = -1
@export var remaining_lifes_start: int = 5

@export var pollen_scene_one: PackedScene
@export var pollen_scene_two: PackedScene
@export var pollen_scene_three: PackedScene
@export var pollen_spawn_height: float = 20.0
@export var allowed_pollen_deviation: float = 5.0
@export var min_pollen_speed: float = 2.0
@export var max_pollen_speed: float = 6.0
@export var min_pollen_eating_time: float = 2.0
@export var max_pollen_eating_time: float = 4.0

@export var music_player: AudioStreamPlayer
@export var nom_player: AudioStreamPlayer
@export var cooking_player: AudioStreamPlayer
@export var build_player: AudioStreamPlayer
@export var fly_player: AudioStreamPlayer

@onready var game_state: GameState = get_node("/root/GameStateSingleton")
@onready var game_manager: GameManager = get_node("/root/GameManagerSingleton")
@onready var helper: Helper = get_node("/root/HelperSingleton")

var current_activity: Activity = Activity.PAUSE:
	set = change_activity

var last_activity: Activity = Activity.SERVE

var current_min_seconds_until_spawn: float = start_min_seconds_until_spawn
var playtime_seconds: float = 0.0
var next_spawn_in_playtime_seconds: float = current_min_seconds_until_spawn
var guests_since_last_stage: int = 0

var current_cooking_station: CookingStation = null
var current_available_recipes: Array[Recipe] = []

var current_interaction_seating: Seating = null

var spawnable_pollen: Array[PackedScene] = []
var trash_active: bool = false

var remaining_lifes: int:
	set = change_life

func _ready():
	spawnable_pollen = [pollen_scene_one, pollen_scene_two, pollen_scene_three]
	game_manager.level_manager = self
	remaining_lifes = remaining_lifes_start


func _process(delta):
	if current_activity == Activity.SERVE or current_activity == Activity.COOK:
		# Check guest spawning
		playtime_seconds += delta
		if playtime_seconds > next_spawn_in_playtime_seconds:
			var success = try_spawn_guest()
			if !success:
				pass
				#remaining_lifes -= 1
		
			if current_activity != Activity.GAME_OVER:
				next_spawn_in_playtime_seconds = get_next_spawn_time(success)
		if current_activity != Activity.GAME_OVER:
			uiManager.update_pollen_countdown(next_spawn_in_playtime_seconds - playtime_seconds)


func cooking_station_entered_by_player(cooking_station: CookingStation):
	player.clear_last_entered_keys()
	current_cooking_station = cooking_station
	current_activity = Activity.COOK
	current_available_recipes = cafe.get_possible_recipes(current_cooking_station.recipe_type)
	ui_manager.update_displayed_recipe_instuctions(current_available_recipes)


func cooking_station_exited_by_player(cooking_station: CookingStation):
	if current_cooking_station == cooking_station:
		current_activity = Activity.SERVE
		current_available_recipes = []
		ui_manager.update_displayed_recipe_instuctions(current_available_recipes)


func start_level():
	music_player.play()
	current_activity = Activity.SERVE


func change_activity(activity: Activity):
	last_activity = current_activity
	current_activity = activity
	uiManager.change_activity(activity)


func change_life(lifes: int):
	remaining_lifes = lifes
	if lifes <= 0:
		game_over()
	elif uiManager != null:
		uiManager.update_lifes(lifes)


func try_spawn_guest():
	var seating = cafe.find_free_seating()
	if seating:
		spawn_guest(seating)
		fly_player.play()
		return true
	return false


func spawn_guest(seating: Seating):
	var pollen_eating_time = randf_range(min_pollen_eating_time, max_pollen_eating_time)
	seating.spawn_guests(spawnable_pollen, allowed_pollen_deviation, pollen_spawn_height, min_pollen_speed, max_pollen_speed, pollen_eating_time)


func get_next_spawn_time(last_spawn_successful: bool):
	if last_spawn_successful:
		guests_since_last_stage += 1
		if guests_since_last_stage >= guests_per_stage:
			guests_since_last_stage = 0
			current_min_seconds_until_spawn = max(current_min_seconds_until_spawn + time_change_per_stage, min_min_seconds_until_spawn)



	var time_until_guest = ceil(randf_range(current_min_seconds_until_spawn - 0.999, current_min_seconds_until_spawn + seconds_until_spawn_range))
	return next_spawn_in_playtime_seconds + time_until_guest


func game_over():
	current_activity = Activity.GAME_OVER


func player_entered_key(last_entered_keys):
	if current_activity == Activity.COOK:
		for available_recipe in current_available_recipes:
			if available_recipe as KeyRecipe:
				var status = (available_recipe as KeyRecipe).check_status(last_entered_keys)
				if(status < 0):
					cook(available_recipe)
					return
				else:
					ui_manager.update_status_key_recipe(available_recipe, status)


func cook(recipe: Recipe):
	ui_manager.update_displayed_recipe_instuctions(current_available_recipes)
	player.clear_last_entered_keys()
	player.add_to_inventory(recipe)
	cooking_player.play()


func player_entered_seating(seating: Seating):
	if seating.bought:
		current_interaction_seating = seating
		if seating.ready_to_order:
			ui_manager.update_displayed_order(seating.order)


func player_exited_seating(seating: Seating):
	if current_interaction_seating == seating:
		current_interaction_seating = null
		ui_manager.remove_displayed_order()


func seating_changed_order(seating: Seating):
	if seating == current_interaction_seating:
		ui_manager.update_displayed_order(seating.order)


func player_wants_interaction():
	if current_interaction_seating != null:
		var order_complete = player.check_if_order_complete(current_interaction_seating.order)
		if order_complete:
			deliver_order()
	if trash_active:
		player.remove_last_from_inventory()


func deliver_order():
	ui_manager.remove_displayed_order()
	game_state.money += helper.get_order_value(current_interaction_seating.order)
	player.remove_order(current_interaction_seating.order)
	current_interaction_seating.order_delivered()
	ui_manager.update_money(game_state.money)
	nom_player.play()


func toggle_build_mode():
	if current_activity == Activity.BUILD:
		exit_build_mode()
	elif current_activity != Activity.PAUSE:
		enter_build_mode()


func toggle_pause():
	if current_activity == Activity.PAUSE:
		stop_pause()
	elif current_activity == Activity.BUILD:
		exit_build_mode()
	else:
		pause()


func enter_build_mode():
	game_state.paused = true
	current_activity = LevelManager.Activity.BUILD
	ui_manager.enable_build_mode()
	cafe.enable_build_mode()


func exit_build_mode():
	game_state.paused = false
	current_activity = last_activity
	ui_manager.disable_build_mode()
	cafe.disable_build_mode()


func pause():
	ui_manager.pause_requested()


func stop_pause():
	ui_manager.pause_end_requested()


func try_to_buy(buyable: Buyable):
	if game_state.money >= buyable.price:
		game_state.money -= buyable.buy()
		ui_manager.update_money(game_state.money)
		build_player.play()
		return true
	return false
 

func select_buyable(buyable: Buyable):
	if current_activity == Activity.BUILD:
		ui_manager.select_buyable(buyable)
		return true
	return false


func enable_trash():
	trash_active = true

func disable_trash():
	trash_active = false
