class_name Buyable
extends Node3D

@export var ghost_mesh: MeshInstance3D
@export var bought_mesh: Node3D

@export var mesh_body: StaticBody3D
@export var interaction_area: Area3D
@export var buy_area: Area3D

@export var item_name: String
@export var description: String
@export var price: float

@export var ghost_material: BaseMaterial3D
@export var ghost_material_selected: BaseMaterial3D

var bought: bool

var cafe: Cafe


func _ready():
	buy_area.input_event.connect(_on_buy_area_input)


func _on_buy_area_input(_camera: Camera3D, event: InputEvent, _position: Vector3, _normal: Vector3, _s_hape_idx : int):
	if event.is_pressed() and !event.is_echo() and !bought:
		var success = cafe.select_buyable(self)
		if success:
			ghost_mesh.material_override = ghost_material_selected


func set_parent_cafe(parent_cafe: Cafe):
	cafe = parent_cafe


func buy():
	bought = true
	if ghost_mesh != null:
		ghost_mesh.visible = false
	if bought_mesh != null:
		bought_mesh.visible = true
	if mesh_body != null:
		mesh_body.set_collision_layer_value(3, 1)
	if interaction_area != null:
		interaction_area.set_collision_mask_value(1, 1)
	return price


func enable_ghost():
	if not bought:
		ghost_mesh.visible = true


func disable_ghost():
	if ghost_mesh != null:
		ghost_unselected()
		ghost_mesh.visible = false


func ghost_unselected():
	ghost_mesh.material_override = ghost_material
