class_name Cafe
extends Node
 

@export var level_manager: LevelManager

# Because of Godot array bug every upgrade is seperatly listed
@export var hot_beverage_station: CookingStation
@export var cold_beverage_station: CookingStation
@export var bake_station: CookingStation

@export var table_one: Seating
@export var table_two: Seating
@export var table_three: Seating
@export var table_four: Seating
@export var table_five: Seating

@export var plant_one: Decoration
@export var plant_two: Decoration
@export var plant_three: Decoration
@export var plant_four: Decoration
@export var plant_five: Decoration
@export var plant_six: Decoration
@export var plant_seven: Decoration
@export var plant_eight: Decoration

@export var max_order_size: int = 2

var all_stations: Array[CookingStation] = []
var all_seatings: Array[Seating] = []
var all_decorations: Array[Decoration] = []
var all_buyables: Array[Buyable] = []


func _ready():
	all_stations.append(hot_beverage_station)
	all_stations.append(cold_beverage_station)
	all_stations.append(bake_station)

	all_seatings.append(table_one)
	all_seatings.append(table_two)
	all_seatings.append(table_three)
	all_seatings.append(table_four)
	all_seatings.append(table_five)

	all_decorations.append(plant_one)
	all_decorations.append(plant_two)
	all_decorations.append(plant_three)
	all_decorations.append(plant_four)
	all_decorations.append(plant_five)
	all_decorations.append(plant_six)
	all_decorations.append(plant_seven)
	all_decorations.append(plant_eight)

	all_buyables.append_array(all_stations)
	all_buyables.append_array(all_seatings)
	all_buyables.append_array(all_decorations)

	for buyable in all_buyables:
		buyable.set_parent_cafe(self)

	hot_beverage_station.custom_buy(true)
	table_one.buy()
	plant_one.buy()


# GODOT BUG WITH EXPORTED ARRAY -> RECIPES HARD CODED
func get_possible_recipes(type: Recipe.RecipeType):
	var recipes: Array[Recipe] = []

	match type:
		Recipe.RecipeType.HOT:
			if hot_beverage_station.bought or hot_beverage_station.upgrade_available:
				recipes = [get_node("/root/CoffeeSingleton") as Recipe, get_node("/root/TeaSingleton") as Recipe, get_node("/root/CocoaSingleton") as Recipe, get_node("/root/ChaiSingleton") as Recipe]
				recipes.resize(4 if hot_beverage_station.upgraded else 2)
		Recipe.RecipeType.COLD:
			if cold_beverage_station.bought or cold_beverage_station.upgrade_available:
				recipes = [get_node("/root/SodaSingleton") as Recipe, get_node("/root/ColaSingleton") as Recipe, get_node("/root/JuiceSingleton") as Recipe, get_node("/root/IceteaSingleton") as Recipe]
				recipes.resize(4 if cold_beverage_station.upgraded else 2)
		Recipe.RecipeType.BAKE:
			if bake_station.bought or bake_station.upgrade_available:
				recipes = [get_node("/root/DonutSingleton") as Recipe, get_node("/root/MuffinSingleton") as Recipe, get_node("/root/CakeSingleton") as Recipe, get_node("/root/PieSingleton") as Recipe]
				recipes.resize(4 if bake_station.upgraded else 2)
		Recipe.RecipeType.FROZEN:
			recipes = []
	
	return recipes


func get_all_possible_recipes():
	var recipes: Array[Recipe] = []
	for recipe_type_key in Recipe.RecipeType.keys():
		recipes.append_array(get_possible_recipes(Recipe.RecipeType[recipe_type_key]))

	return recipes


func find_free_seating():
	var free_seatings: Array[Seating] = []

	for seating in all_seatings:
		if seating.bought and not seating.occupied:
			free_seatings.append(seating)
	
	if free_seatings.size() == 0:
		return false
	
	return free_seatings[randi() % free_seatings.size()]


func generate_order():
	var order: Array[Recipe] = []
	var order_size: int = randi_range(1, max_order_size)
	var all_possible_recipes = get_all_possible_recipes()
	for i in range(0, order_size):
		if all_possible_recipes.size() > 0:
			var recipe_index = randi_range(0, all_possible_recipes.size() - 1)
			order.append(all_possible_recipes[recipe_index])
			all_possible_recipes.remove_at(recipe_index)

	return order


func seating_changed_order(seating: Seating):
	level_manager.seating_changed_order(seating)


func player_entered_seating(seating: Seating):
	level_manager.player_entered_seating(seating)


func player_exited_seating(seating: Seating):
	level_manager.player_exited_seating(seating)


func cooking_station_entered_by_player(cooking_station: CookingStation):
	level_manager.cooking_station_entered_by_player(cooking_station)


func cooking_station_exited_by_player(cooking_station: CookingStation):
	level_manager.cooking_station_exited_by_player(cooking_station)


func enable_build_mode():
	for buyable in all_buyables:
		buyable.enable_ghost()
		

func disable_build_mode():
	for buyable in all_buyables:
		buyable.disable_ghost()


func select_buyable(buyable: Buyable):
	return level_manager.select_buyable(buyable)
