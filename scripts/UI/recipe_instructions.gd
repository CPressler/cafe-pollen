class_name RecipeInstructions
extends Panel


@onready var helper: Helper = get_node("/root/HelperSingleton")


func update_status(correct: int):
	var recipe_instructions = get_node("RecipeInstructions")
	if recipe_instructions != null:
		var children = recipe_instructions.get_children()
		for i in range(0, children.size()):
			var arrow_texture = children[i] as TextureRect
			if arrow_texture:
				var color: bool = correct > i
				arrow_texture.get_child(0).visible = color


func hide_steps():
	var recipe_instructions = get_node("RecipeInstructions")
	if recipe_instructions != null:
		recipe_instructions.visible = false