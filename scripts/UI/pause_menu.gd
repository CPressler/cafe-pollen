class_name PauseMenu
extends Control


@export var ui_manager: UIManager
@export var continue_button: Button
@export var exit_button: Button


func _ready():
    continue_button.pressed.connect(_on_continue_button_pressed)
    exit_button.pressed.connect(_on_exit_button_pressed)


func _on_continue_button_pressed():
    ui_manager.pause_end_requested()


func _on_exit_button_pressed():
    get_tree().quit()