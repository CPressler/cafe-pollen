class_name UIManager
extends Control


@export var level_manager: LevelManager

@export var intro_menu: IntroMenu
@export var pause_menu: PauseMenu
@export var hud: HUD

@onready var game_state: GameState = get_node("/root/GameStateSingleton")


func change_activity(activity: LevelManager.Activity):
	hide_all_menus()
	match activity:
		LevelManager.Activity.PAUSE:
			pause_menu.show()
			hud.hide()
		LevelManager.Activity.SERVE:
			hud.show()
		LevelManager.Activity.COOK:
			hud.show()
		LevelManager.Activity.BUILD:
			hud.show()


func hide_all_menus():
	intro_menu.hide()
	pause_menu.hide()


func update_lifes(life: int):
	hud.update_lifes(life)


func update_pollen_countdown(seconds_remaining: float):
	hud.update_pollen_countdown(seconds_remaining)


func update_displayed_recipe_instuctions(recipes: Array[Recipe]):
	hud.update_displayed_recipe_instuctions(recipes)


func update_status_key_recipe(recipe: KeyRecipe, correct: int):
	hud.update_status_key_recipe(recipe, correct)


func add_to_inventory(recipe: Recipe):
	hud.add_to_inventory(recipe)


func remove_from_inventory(index: int):
	hud.remove_from_inventory(index)


func clear_inventory():
	hud.clear_inventory()


func update_displayed_order(order: Array[Recipe]):
	hud.update_displayed_order(order)


func remove_displayed_order():
	hud.clear_instructions()


func update_money(money: float):
	hud.update_money(money)


func pause_requested():
	game_state.paused = true
	level_manager.current_activity = LevelManager.Activity.PAUSE
	hud.visible = false
	pause_menu.visible = true


func pause_end_requested():
	game_state.paused = false
	level_manager.current_activity = level_manager.last_activity
	hud.visible = true
	pause_menu.visible = false


func build_mode_requested():
	level_manager.enter_build_mode()


func enable_build_mode():
	hud.activate_build_mode()


func build_mode_end_reqested():
	level_manager.exit_build_mode()


func disable_build_mode():
	hud.end_build_mode()


func try_to_buy(buyable: Buyable):
	return level_manager.try_to_buy(buyable)


func select_buyable(buyable: Buyable):
	hud.switch_displayed_buyable(buyable)
