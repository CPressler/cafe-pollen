class_name IntroMenu
extends Control

@export var chill_mode_button: Button
@export var challenge_mode_button: Button
@onready var game_manager: GameManager = get_node("/root/GameManagerSingleton")

func _ready():
    chill_mode_button.pressed.connect(_on_chill_mode_button_press)
    challenge_mode_button.pressed.connect(_on_challenge_mode_button_press)

func _on_chill_mode_button_press():
    game_manager.start_game(GameManager.GameMode.CHILL)


func _on_challenge_mode_button_press():
    game_manager.start_game(GameManager.GameMode.CHALLENGE)