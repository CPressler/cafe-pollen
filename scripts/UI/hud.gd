class_name HUD
extends Control

@export var ui_manager: UIManager

@export var cook_mode_ui: Control
@export var buy_mode_ui: Control

@export var money_label: RichTextLabel
@export var back_button: Button

@export var interaction_container: HBoxContainer
@export var inventory_container: HBoxContainer
@export var life_count: RichTextLabel
@export var timer_text: RichTextLabel
@export var build_button: Button

@export var buy_name: RichTextLabel
@export var buy_description: RichTextLabel
@export var buy_price: RichTextLabel
@export var buy_button: Button

@onready var helper: Helper = get_node("/root/HelperSingleton")
@onready var game_state: GameState = get_node("/root/GameStateSingleton")

var currently_displayed_instructions: Dictionary
var currently_displayed_inventory: Array[Node]

var currently_selected_buyable: Buyable

var build_mode_active = false

func _ready():
	back_button.pressed.connect(_on_back_button_pressed)
	build_button.pressed.connect(_on_build_button_pressed)
	buy_button.pressed.connect(_on_buy_button_pressed)

func _on_back_button_pressed():
	if build_mode_active:
		ui_manager.build_mode_end_reqested()
	else:
		ui_manager.pause_requested()


func _on_build_button_pressed():
	ui_manager.build_mode_requested()


func _on_buy_button_pressed():
	if currently_selected_buyable != null:
		var success = ui_manager.try_to_buy(currently_selected_buyable)
		if success:
			clear_selected_buyable()


func update_lifes(lifes: int):
	life_count.text = helper.center_text(str(lifes))


func update_pollen_countdown(seconds_remaining: float):
	timer_text.text = helper.center_text(str(ceil(seconds_remaining)))


func update_displayed_recipe_instuctions(recipes: Array[Recipe]):
	clear_instructions()

	for recipe in recipes:
		var instructions = recipe.instruction_ui.instantiate()
		interaction_container.add_child(instructions)
		currently_displayed_instructions[recipe.name] = instructions


func update_displayed_order(order: Array[Recipe]):
	clear_instructions()

	for recipe in order:
		var instructions = recipe.instruction_ui.instantiate()
		interaction_container.add_child(instructions)
		currently_displayed_instructions[recipe.name] = instructions
		(instructions as RecipeInstructions).hide_steps()


func clear_instructions():
	currently_displayed_instructions.clear()
	for child in interaction_container.get_children():
		child.queue_free()


func update_status_key_recipe(recipe: KeyRecipe, correct: int):
	var displayed_instructions = currently_displayed_instructions[recipe.name]
	if displayed_instructions as RecipeInstructions:
		displayed_instructions.update_status(correct)


func add_to_inventory(recipe: Recipe):
	var inventory_item = recipe.inventory_ui.instantiate()
	inventory_container.add_child(inventory_item)
	currently_displayed_inventory.append(inventory_item)


func remove_from_inventory(index: int):
	if currently_displayed_inventory.size() > index:
		var item_to_delete = currently_displayed_inventory.pop_at(index)
		item_to_delete.queue_free()


func clear_inventory():
	while currently_displayed_inventory.size() > 0:
		remove_from_inventory(currently_displayed_inventory.size() - 1)


func update_money(money: float):
	money_label.text = helper.center_text(helper.format_float(money))


func activate_build_mode():
	build_mode_active = true
	buy_mode_ui.visible = true
	cook_mode_ui.visible = false


func end_build_mode():
	build_mode_active = false
	buy_mode_ui.visible = false
	cook_mode_ui.visible = true
	clear_selected_buyable()


func clear_selected_buyable():
	currently_selected_buyable = null
	buy_name.text = ""
	buy_description.text = ""
	buy_price.text = ""
	buy_button.disabled = true
	buy_button.text = ""


func switch_displayed_buyable(buyable: Buyable):
	if currently_selected_buyable != null:
		currently_selected_buyable.ghost_unselected()

	currently_selected_buyable = buyable
	if game_state.money >= buyable.price:
		allow_buy()
	else:
		deny_buy()
	
	buy_name.text = helper.center_text(helper.bold_text(buyable.item_name))
	buy_description.text = helper.center_text(buyable.description)
	buy_price.text = helper.center_text(helper.get_currency_text(helper.format_float(buyable.price)))


func allow_buy():
	buy_button.disabled = false
	buy_button.text = "Buy!"


func deny_buy():
	buy_button.disabled = true
	buy_button.text = "Too Expensive"
