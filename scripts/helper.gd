class_name Helper
extends Node


var center_format: String = "[center]%s[/center]"
var color_format: String = "[color=%s]%s[/color]"
var bold_format: String = "[b]%s[/b]"
var currency_format: String = "%s SC"


func center_text(text):
    return center_format % text


func color_text(text, color):
    return color_format % [color, text]


func bold_text(text):
    return bold_format % text


func remove_bbcode(text: String):
    var regex = RegEx.new()
    regex.compile("\\[.*?\\]")
    return regex.sub(text, "", true)


func format_float(float_to_format: float):
    return str(float_to_format).pad_decimals(2)


func get_order_value(order: Array[Recipe]):
    var value: float = 0.0
    for recipe in order:
        value += recipe.price
    return value


func get_currency_text(text):
    return currency_format % text