class_name CookingStation
extends Buyable


@export var upgrade_mesh: Node3D

@export var recipe_type: Recipe.RecipeType
var upgrade_available: bool = false
var upgraded: bool = false


func _ready():
	super()
	interaction_area.body_entered.connect(_on_interaction_area_body_entered)
	interaction_area.body_exited.connect(_on_interaction_area_body_exited)


func custom_buy(force_hide_ghost: bool = false):
	self.buy()
	if force_hide_ghost:
		ghost_mesh.visible = false


func buy():
	if(upgrade_available):
		upgrade()
	else:
		super()
		bought = false
		upgrade_available = true
		ghost_mesh.visible = true

	return price


func upgrade():
	bought = true
	upgraded = true
	ghost_mesh.visible = false
	bought_mesh.visible = false
	upgrade_mesh.visible = true


func _on_interaction_area_body_entered(_body: Node3D):
	cafe.cooking_station_entered_by_player(self)


func _on_interaction_area_body_exited(_body: Node3D):
	cafe.cooking_station_exited_by_player(self)
