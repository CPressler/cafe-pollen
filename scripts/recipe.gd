class_name Recipe
extends Node


enum RecipeType {
    HOT,
    COLD,
    BAKE,
    FROZEN
}


@export var instruction_ui: PackedScene
@export var inventory_ui: PackedScene

@export var recipe_type: RecipeType
@export var recipe_name: String
@export var price: float = 1.0