class_name Trash
extends Node3D


@export var interaction_area: Area3D
@export var level_manager: LevelManager


func _ready():
	interaction_area.body_entered.connect(_on_interaction_area_body_entered)
	interaction_area.body_exited.connect(_on_interaction_area_body_exited)


func _on_interaction_area_body_entered(_body: Node3D):
	level_manager.enable_trash()


func _on_interaction_area_body_exited(_body: Node3D):
	level_manager.disable_trash()
