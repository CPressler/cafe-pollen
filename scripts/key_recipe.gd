class_name KeyRecipe
extends Recipe

@export var instructions: Array[String]


func check_status(input: Array[String]):
    var current_position = 0

    for i in range(0, input.size()):
        if input[i] != instructions[current_position]:
            current_position = 0
        else:
            current_position += 1
            if current_position == instructions.size():
                return -1

    return current_position