class_name GameManager
extends Node


enum GameMode {
	CHILL,
	CHALLENGE
}

signal game_started(mode: GameMode)

@onready var game_state: GameState = get_node("/root/GameStateSingleton")

var level_manager: LevelManager

func _ready():
	randomize()


func start_game(mode: GameMode):	
	game_state.mode = mode

	if mode == GameMode.CHILL:
		emit_signal("game_started", GameMode.CHILL)
	elif mode == GameMode.CHALLENGE:
		emit_signal("game_started", GameMode.CHALLENGE)

	level_manager.start_level()
